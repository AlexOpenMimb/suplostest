<?php

require_once "../model/Conection.php";
class City
{
    private static $conetion;
    private static $cities = [];

    public static function getConetion(){
        self::$conetion = Conection::conectar();
    }

    public static function getCities(){
        self::getConetion();

        $query = "SELECT *  FROM cities";

        $city = mysqli_query(self::$conetion, $query);

        while ($row = $city->fetch_assoc()) {
            self::$cities[]=$row;
        }     
        return self::$cities;            
    }        
}


?>