<?php

require_once "../model/Conection.php";
require_once "../model/Property.php";


class MyProperty
{
    private static $conection;
    private static $myProperties = [];
   

    public static function getConetion(){
        self::$conection = Conection::conectar();
    }

    public static function getMyProperty(){
        self::getConetion();

        $query = "SELECT mp.id, p.address, c.city, p.phone, p.postal_code, t.type, p.price  FROM my_propertys AS mp INNER JOIN propertys AS p ON mp.id_property = p.id INNER JOIN cities AS c ON p.id_city = c.id INNER JOIN type_of_propertys AS t ON p.id_type = t.id";

        $property = mysqli_query(self::$conection, $query);

        while ($row = $property->fetch_assoc()) {

            self::$myProperties[]=$row;
        }     
        return self::$myProperties;            
    }

    public static function deleteProperty($id){  
        self::getConetion();

        if (Property::validate($id) == false) {
            $query = "DELETE FROM my_propertys WHERE id = $id";
            $property = mysqli_query(self::$conection, $query);
            if ($property) {
                return true;
            }
        }
        return false;           
    } 


            
}

