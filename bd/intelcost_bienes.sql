-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-10-2022 a las 18:43:52
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `intelcost_bienes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(10) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `city`) VALUES
(1, 'New York'),
(2, 'Washington'),
(4, 'Los Angeles'),
(5, 'Miami'),
(6, 'Orlando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `my_propertys`
--

CREATE TABLE `my_propertys` (
  `id` int(10) NOT NULL,
  `id_property` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propertys`
--

CREATE TABLE `propertys` (
  `id` int(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `id_city` int(10) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `id_type` int(10) NOT NULL,
  `price` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `propertys`
--

INSERT INTO `propertys` (`id`, `address`, `id_city`, `phone`, `postal_code`, `id_type`, `price`) VALUES
(1, 'Ap #549-7395 Ut Rd.', 1, '334-052-0954', '85328', 1, '$30,746'),
(2, 'P.O. Box 657, 9831 Cursus St.', 6, '488-441-5521', '04436', 2, '$71,045'),
(3, 'Ap #325-2507 Quisque Av.', 4, '623-807-2869', '89804', 2, '$36,087'),
(4, '347-866 Laoreet Road', 4, '997-640-8188', '94526-134', 2, '$16,048'),
(5, '672-9576 Consectetuer Road', 6, '355-601-5749', '210020', 2, '$64,370'),
(6, 'P.O. Box 847, 2589 In Av.', 2, '390-713-8687', '70689', 4, '$60,951'),
(7, 'P.O. Box 497, 8679 Turpis. St.', 1, '870-559-3430', '7029', 1, '$17,759');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_of_propertys`
--

CREATE TABLE `type_of_propertys` (
  `id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `type_of_propertys`
--

INSERT INTO `type_of_propertys` (`id`, `type`) VALUES
(1, 'Casa'),
(2, 'Casa de Campo'),
(4, 'Apartamento');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `my_propertys`
--
ALTER TABLE `my_propertys`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `propertys`
--
ALTER TABLE `propertys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_city` (`id_city`),
  ADD KEY `id_type` (`id_type`);

--
-- Indices de la tabla `type_of_propertys`
--
ALTER TABLE `type_of_propertys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `my_propertys`
--
ALTER TABLE `my_propertys`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `propertys`
--
ALTER TABLE `propertys`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `type_of_propertys`
--
ALTER TABLE `type_of_propertys`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
