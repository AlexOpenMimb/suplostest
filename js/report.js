$(document).ready(function(){

    

    $("#btnExcelExport").click(function(){

        if($('#tableBody').length == 0){
         
            $('#errorGenerateiD').text('No hay datos suficientes para generar el reporte')   
            setTimeout(() => {
                $('#errorGenerateiD').text('')                    
            }, 2000);

        }else{
            $("#tableExcel").table2excel({
             
                exclude: ".noExl",
                name: "Worksheet Name",
                filename: "Reporte", 
                fileext: ".xls" 
              }); 

        }

        
      });
      
})