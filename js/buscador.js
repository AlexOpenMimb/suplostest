//Inizializar el objeto para el buscador
let object;

$(document).ready(function(){
    data()
      
})

//Cargar los datos en la carga del documento
async function data(){
    await getCities();
    await getType();  
    await getProperty();  
    
}

//Ciudad
async function getCities(){

    $.ajax({
        url: 'route/requests.php?c=getcities',
        success:function(data){
           
            data.forEach(element => {
                $('#selectCiudad, #selectCiudadExcel').append(`
                    <option value="${element.id}">${element.city}</option>
                `);
            });
        },
        error:function(error){
            console.log(error)
        }
    });

}

//Tipo de propiedad
async function getType(){

    $.ajax({
        url: 'route/requests.php?t=getypes',
        success:function(data){
           
            data.forEach(element => {
                $('#selectTipo, #selectTipoExcel').append(`
                    <option value="${element.id}">${element.type}</option>
                `);
            });
        },
        error:function(error){
            console.log(error)
        }
    });

}

//Propiedades guardadas
async function getMyProperty(){

    $('#MypropertyContainer').html('')

    $.ajax({
        url: 'route/requests.php?mp=getproperty',
        success:function(data){
           
            if (data.length > 0) {

                data.forEach(element => {
                    $('#MypropertyContainer').append(`
                    <div  class="property-card">
                    <img class="mx-auto" src="img/home.jpg" alt="">
                    <div class="d-flex f-column m-y">
                        <span>Ciudad: ${element.city}</span>
                        <span>Codigo postal: ${element.postal_code}</span>
                        <span>Direccion: ${element.address}</span>
                        <span>Telefono: ${element.phone}</span>
                        <span>Tipo: ${element.type}</span>
                        <span>Precio: ${element.price}</span>
                        <button class="w-7" onclick="deleteProperty(${element.id})">Eliminar</button> 
                    </div>
                    </div>              
                    `);
                });
            }else{
                $('#MypropertyContainer').html(`                
                    <div  class="property-card">
                        <div class="c-red">                            
                            <span>No hay propiedades guardadas</span>
                        </div>
                    </div> 
                `)
            }            
        },
        error:function(error){
            console.log(error)
        }
    });

}

//Listar las propiedadess
async function getProperty(){
    $('#propertyContainer').html('');
    $.ajax({
        url: 'route/requests.php?p=getproperties',
        success:function(data){

            data.forEach(element => {
                
                $('#propertyContainer').append(`

                <div  class="property-card">
                <img class="mx-auto" src="img/home.jpg" alt="">
                <div class="d-flex f-column m-y">
                    <span>Ciudad: ${element.city}</span>
                    <span>Codigo postal: ${element.postal_code}</span>
                    <span>Direccion: ${element.address}</span>
                    <span>Telefono: ${element.phone}</span>
                    <span>Tipo: ${element.type}</span>
                    <span>Precio: ${element.price}</span>
                    <button class="w-7" onclick="saveProperty(${element.id})">Guardar</button>
                    <span id="message${element.id}" class="c-green"></span>
                    <span id="message-error${element.id}" class="c-red"></span>
                </div>
             </div>
                `);
            });
        },
        error:function(error){
            console.log(error)
        }
    });

}

//Almacenar las propiedades
function saveProperty(id){

    $.ajax({
        url: 'route/requests.php?p=saveproperty',
        data: {
            id
        },
        method: "POST",
        success: function (response) {
            console.log(response);
            getMyProperty();
            if (response) {
                $('#message' + id).text('Registro guardado con Éxito')
                setTimeout(() => {
                    $('#message' + id).text('')                    
                }, 1000);    
            }else{
                $('#message-error' + id).text('El registro ya ha sido guardado')    
                setTimeout(() => {
                    $('#message-error' + id).text('')                    
                }, 1000);  
            }
        },
        error:function(x){
            window.open(JSON.stringify(x));
        }
    });


}

//Eliminar las propiedades 
function deleteProperty(id){
    $.ajax({
        url: 'route/requests.php?mp=deleteproperty',
        data: {
            id
        },
        method: "POST",
        success: function (response) {
            console.log(response);
            if (response) {
               
                getMyProperty();

                $('#messageDelete').text('Elimnado correctamente')
                setTimeout(() => {
                    $('#messageDelete').text('')                    
                }, 1000);    
            }else{
                $('#messageDelete').text('Ha ocurrido un error')    
                setTimeout(() => {
                    $('#messageDelete').text('')                    
                }, 1000);  
            }
        },
        error:function(x,xs,xt){
            window.open(JSON.stringify(x));
        }
    });
}

//Buscador
function searcher() {

    let city = $( "#selectCiudad option:selected" ).val()
    let type = $( "#selectTipo option:selected" ).val()

            if (city === "all" && type === "all") {
        
                getProperty();
        
            }else if(city !== "all" && type !== "all"){
        
                object = {
                    city,
                    type,  
                }     
                render();
            }
  
}


//Renderizar datos
function render(){
   
    $('#propertyContainer').html('');

    $.ajax({
        url: 'route/requests.php?p=searchproperty',
        data: object,
        method: "POST",
        success: function (data) {
            console.log(data);

            if (data.length > 0) {

                data.forEach(element => {

                    $('#propertyContainer').append(`  
                            <div  class="property-card">
                            <img class="mx-auto" src="img/home.jpg" alt="">
                            <div class="d-flex f-column m-y">
                                <span>Ciudad: ${element.city}</span>
                                <span>Codigo postal: ${element.postal_code}</span>
                                <span>Direccion: ${element.address}</span>
                                <span>Telefono: ${element.phone}</span>
                                <span>Tipo: ${element.type}</span>
                                <span>Precio: ${element.price}</span>
                                <button class="w-7" onclick="saveProperty(${element.id})">Guardar</button>
                                <span id="message${element.id}" class="c-green"></span>
                                <span id="message-error${element.id}" class="c-red"></span>
                            </div>
                        </div>                   
                    `);
                });
            }else{
                $('#propertyContainer').html(`                
                    <div  class="property-card">
                        <div class="c-red">                            
                            <span>No exiten propiedades con esos parametros de busqueda</span>
                        </div>
                    </div> 
                `)
            }
        },
        error:function(x){
            window.open(JSON.stringify(x));
        }
    });

}

//Generar reporte
function reportExcel(){
    let city = $( "#selectCiudadExcel option:selected" ).val()
    let type = $( "#selectTipoExcel option:selected" ).val()
    if (city !== "" && type !== "") {
        object = {
            city,
            type,  
        }
        generateExcel();
      
    }else{
        $('#errorReportiD').text('Faltan campos por selecionar')   
        setTimeout(() => {
            $('#errorReportiD').text('')                    
        }, 2000);
    }
}

//Generar archivo excel
function generateExcel(){
    $('#excelContainer').html('');
    $.ajax({
        url: 'route/requests.php?p=searchproperty',
        data: object,
        method: "POST",
        success: function (data) {
            console.log(data);

            if (data.length > 0) {

                data.forEach(element => {
                     
                    $('#excelContainer').append(`  
                    <tr id="tableBody">
                        <td class="border">${element.city}</td>
                        <td class="border">${element.postal_code}</td>
                        <td class="border">${element.address}</td>
                        <td class="border">${element.phone}</td>
                        <td class="border">${element.type}</td>
                        <td class="border">${element.price}</td>
                      </tr>
                    `);

                    $('#successReportiD').text('Reporte generado con éxito')   
                    setTimeout(() => {
                        $('#successReportiD').text('')                    
                    }, 2000);

                });
            }else{
                $('#errorReportiD').text('No existen datos con los campos selecionados')   
                setTimeout(() => {
                    $('#errorReportiD').text('')                    
                }, 2000);
            }
        },
        error:function(x){
            window.open(JSON.stringify(x));
        }
    });
}





