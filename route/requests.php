<?php

header("Content-type: application/json");
include "../controllers/CityController.php";
include "../controllers/TypePropertyController.php";
include "../controllers/PropertyController.php";
include "../controllers/MyPropertyController.php";


$url = $_SERVER["REQUEST_URI"];   
$array = explode('?', $url);

$query = $array[count($array) - 1];
$arrayQuery = explode('=', $query);

$controller  = $arrayQuery[0];
$method      = $arrayQuery[1];



if($_SERVER["REQUEST_METHOD"] == "GET"){

    switch ($controller."=".$method) {

        case "c=getcities":
            if( $cities = CityController::getCities()){                        
                echo  json_encode($cities);
            }else{
                echo json_encode([]);
               
            }
            break;

        case "t=getypes":
            if( $type = TypePropertyController::getTypeProperty()){                        
                echo  json_encode($type);
            }else{
                echo json_encode([]);
               
            }
            break;

        case "p=getproperties":
            if( $property = PropertyController::getProperty()){                        
                echo  json_encode($property);
            }else{
                echo json_encode([]);
               
            }
            break;

        case "mp=getproperty":
            if( $Myproperty = MyPropertyController::getMyProperty()){                        
                echo  json_encode($Myproperty);
            }else{
                echo json_encode([]);
               
            }
            break;
       
    }       
}


if($_SERVER["REQUEST_METHOD"] == "POST"){

    switch ($controller."=".$method) {
        case 'p=searchproperty':
            
            if( $type = PropertyController::searcherProperty($_POST["city"], $_POST["type"])){    
                echo json_encode($type);
            }else{
                echo json_encode([]);
            }
            break;
        case 'p=saveproperty':
            
            if( $type = PropertyController::saveProperty($_POST["id"])){    
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
            break;
        case 'mp=deleteproperty':
            
            if( $type = MyPropertyController::deleteProperty($_POST["id"])){    
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
            break;
      
    }
}


?>